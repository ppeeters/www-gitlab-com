### Consolidate Slack Integrations into a Simplified and Upgraded Slack Application

Slack is one of our most heavily used integrations, is leveraged by customers large and small, and is our primary chat solution outside of GitLab To-Dos and Notifications, which allows for dogfooding and rapid feedback loops.

Today we have multiple Slack integrations, but our goal is to consolidate and simplify our integrations into a single Slack Application that serves GitLab.com and GitLab Self-Managed customers, making it easier for users to access the full set of features, as well as to improve the efficiency of our development/maintenance. 

See our [planning epic](https://gitlab.com/groups/gitlab-org/-/epics/7536).

### Improve our Webhooks

Webhooks are a generic way for projects to be integrated with any other service. GitLab's APIs allow other services to _reach in to_ our data, Webhooks proactively send data to another service when certain events happen. These are increasingly important for external vendors, as they offer a key way to integrate with GitLab that doesn't require them building inside our codebase. Webhooks also give users, customers, and partners a more efficient pattern for receiving data triggered based on events, rather than inefficiently polling to see if any new events are available.

Our top priorities in this category are:

* [Adding notifications for failed webhooks to group webhooks](https://gitlab.com/gitlab-org/gitlab/-/issues/385902)
* [Add networking filter for outbound requests for webhooks and integrations](https://gitlab.com/gitlab-org/gitlab/-/issues/377371)
* [Generate audit events for webhooks and integrations](https://gitlab.com/groups/gitlab-org/-/epics/8710)
* [Add webhook metadata](https://gitlab.com/gitlab-org/gitlab/-/issues/353562)
* [Disable Invalid Webhooks](https://gitlab.com/groups/gitlab-org/-/epics/8198)

See our [planning epic](https://gitlab.com/groups/gitlab-org/-/epics/2318).

### Improve Jira Integration support

Jira is one of our most popular integrations, and a common thread we hear is that "developers want
to be able to stay in GitLab", and not need to visit Jira to do daily tasks. The goal of our upcoming work is to get the features to a point where a typical developer can stay in GitLab for the majority of their Jira needs.

Self-managed GitLab users will soon be able to use the [GitLab for Jira Cloud app](https://gitlab.com/groups/gitlab-org/-/epics/5650).

[Support for Personal Access Tokens](https://gitlab.com/groups/gitlab-org/-/epics/8222) will give Jira users an alternative to basic authentication, and a more secure pattern for managing credentials for their integration.

Customers are also increasingly interested in [leveraging Jira issue data within GitLab's Value Stream Analytics](https://gitlab.com/gitlab-org/gitlab/-/issues/348626), which will focus on surfacing issue start/close data into GitLab reporting.

### Build a native ServiceNow integration

ServiceNow is a key component in how many of our largest customers handle Change Management. Through ServiceNow, they maintain an audited chain of custody with code changes, approve/deny changes based on a strict approval workflow, and manage deployment on a scheduled cadence. ServiceNow allows these customers to take these audit logs and centralize them with other data that they're using to monitor and report about their compliance regime.

In coordination with ServiceNow as a partner, ServiceNow is actively working to enhance their GitLab Spoke to support more actions for CI/CD, Incident Management, MR Management, and Package Management.

We'll next be exploring an MVC for a native GitLab integration that [enables Ultimate users to facilitate Change Management between GitLab and ServiceNow](https://gitlab.com/groups/gitlab-org/-/epics/8180).

### Create a joint REST and GraphQL API strategy

GitLab offers a REST and GraphQL API to give customers options on how to best integrate with GitLab. Until now, we have not developed a cohesive strategy that optimizes for parity between them and efficiency in maintaining both implementations.

Immediate next steps, as a function of our [API Working Group](https://about.gitlab.com/company/team/structure/working-groups/api-vision/) are:

* Test our [POC for a REST wrapper over our GraphQL API](https://gitlab.com/gitlab-org/gitlab/-/issues/363795), dubbed `Raisin`
* Explore mechanisms for [Automated Documentation](https://gitlab.com/groups/gitlab-org/-/epics/5792), ideally generating OpenAPI specs based on our REST APIs

### Shifting integrations within each domain to the relevant GitLab product group as DRI and providing tooling, process, and support to scale

As we continue to scale how we support integrations at GitLab, we'll be working closely with product teams to shift support and prioritization to the relevant areas of the product. Similar to APIs, webhooks, audit events, authentication, and other horizontal services, integrations are better supported by the teams closest to the source of customer pain and need. Rather than centralized prioritization, this enables teams to think outside of the box about how integrations can take their features to the next level, gain more exposure from new audiences, or integrate strategically with competitors to achieve business outcomes.

To support teams in building integrations, we'll be focusing on the technology and tools product teams will leverage to build integrations, including our APIs, webhooks, our Static Integrations DSL, and further abstractions to simplify the process of building integrations.

To achieve this we'll be focusing on the following areas:

* [Defining ownership of integrations in GitLab](https://about.gitlab.com/direction/manage/integrations/#integration-ownership-draft)
* Adding all existing integrations to our [Integrations Static DSL](https://gitlab.com/groups/gitlab-org/-/epics/7652)
* Improving our review process with [automations to guide new contributors (internal and external)](https://gitlab.com/gitlab-org/gitlab/-/issues/385969)
* Enhancing our [Integrations Development Guide](https://docs.gitlab.com/ee/development/integrations/) to support more use cases
* Simplifying testing and app submission processes so it's less cumbersome for contributors




